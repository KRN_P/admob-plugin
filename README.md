ADMOB-PLUGIN
===================

 A plugin to display native ads in react-native components
   
 **Installation**
 
  Add this line in `dependencies` section of your `package.json` file.
  
   ```"admob-plugin":"https://KRN_P@bitbucket.org/KRN_P/admob-plugin.git"```
   
   Run these commands to integrate the plugin.
   
```npm install```

  ```react-native link```
	
  **How to use**
  **Banner Ads**
  
  ```import { AdMobBanner } from "admob-plugin";
	  ...
	  <AdMobBanner
	          adSize="banner"
	          adUnitID="ca-app-pub-3940256099942544/6300978111"
	          ref={el => (this._adExample = el)}
        />
  ```
You can define the `ref` property to access the methods of `AdMobBanner`  component.

**Props**

  ```adSize: string```
   Default is banner.

  ```adUnitID: string```
  AdMob ad unit ID

  ```testDevices: arrayOf(string)```
  Array of test devices. Use ```AdMobBanner.simulatorId``` for the simulator.
  
  Ad lifecycle events.
  
  ```onAdLoaded: func```
  ```onAdFailedToLoad: func```
  ```onAdOpened: func```
 ``` onAdClosed: func```
  ```onAdLeftApplication: func```

  **AdMob library banner size constants**
 
   - banner (320x50, Standard Banner for Phones and Tablets)
   - largeBanner (320x100, Large Banner for Phones and Tablets)
   - mediumRectangle (300x250, IAB Medium Rectangle for Phones and Tablets)
   - fullBanner (468x60, IAB Full-Size Banner for Tablets)
   - leaderboard (728x90, IAB Leaderboard for Tablets)
   - smartBannerPortrait (Screen width x 32|50|90, Smart Banner for Phones and Tablets)
   * smartBannerLandscape (Screen width x 32|50|90, Smart Banner for Phones and Tablets)
   
**Method**
	
```loadBanner```
To reload the banner ad.

**Interstitial Ads**

Import the AdMobInterstitial.

```import { AdMobInterstitial } from "admob-plugin"```

Set the `adUnitId` and `devices`.
To show a add just call the `showAd` method.

```AdMobInterstitial.showAd().catch(error => console.warn(error));```

**Methods**

```setTestDevices(devices: Array<string>)```

```setAdUnitID(adUnitId: string)```

```addEventListener(listenerType: string, callback: function)```

```requestAd()```

Listener Types:

 - adLoaded
 - adFailedToLoad
 - adOpened
 - adClosed
 - adLeftApplication