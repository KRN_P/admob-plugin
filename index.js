import { NativeModules } from 'react-native';
import AdMobBanner from "./AdMobBanner";
import AdMobInterstitial from "./AdMobInterstitial";

export {
    AdMobBanner,
    AdMobInterstitial
};
